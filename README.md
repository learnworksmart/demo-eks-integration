# Create EKS Cluster with GitLab Kubernetes Cluster integration. 
Refer to this article on [GitLab Kubernetes Clusters integration using AWS EKS](https://about.gitlab.com/blog/2020/03/09/gitlab-eks-integration-how-to/). 

The integration would requires 2 IAM roles: 
1. A **Provision Role** which allows GitLab to provision the required AWS resources. 
1. A **Service Role** which is an [assume role](https://docs.aws.amazon.com/STS/latest/APIReference/API_AssumeRole.html) for AWS EKS.  

## Prerequisites
1. On your local machine, setup the [AWS Credentials File and Credential Profiles](https://docs.aws.amazon.com/sdk-for-php/v3/developer-guide/guide_credentials_profiles.html)  with neccessary access rights to your AWS environment. 
1. Visit GitLab Kubernetes integration page,  **Operations -> Kubernetes -> Create cluster on Amazon EKS**, capture the required **Account ID** and **External ID**. 
![image](/uploads/3716c8d657868769fad21f747ca7e52e/image.png) 
1. Clone this respository
    * Supply both the **Account ID** and **External ID** to our `prerequisites/variables.tf` gitlab variables. 
    * Run `cd prerequisites && terraform init && terraform apply --auto-approve`. 
    * This should create the 2 IAM roles, **gitlab-eks-provision-role** & **gitlab-eks-service-role**
    * Query and capture the arn of our provision role **gitlab-eks-provision-role** with `terraform state show aws_iam_role.gitlab_eks_provision_role | grep arn | grep role`. 

## Create an AWS EKS cluster with GitLab Kubernetes Cluster setup
1. Back to GitLab Kubernetes integration page, under `Provision Role ARN` supply the arn of our **provision role** and the cluster region. Next, click on `Authenticate with AWS` button. 
![image](/uploads/c2c5521e5a5518e6e09dfeea53cab37b/image.png)
1. Enter the details for your Amazon EKS Kubernetes cluster and complete it with `Create Kubernetes cluster` button. Do select the service role created from our terraform script, `gitlab-eks-service-role`.
![image](/uploads/0e4d11bc4c93c366dc97627785a88428/image.png)
1. Wait for the Kubernetes cluster to be created, which will take about 10 minutes. 
![image](/uploads/ba0b904b001d4409f52d9ed743beaef1/image.png)
1. On the same local machine, registered the newly created EKS cluster with `aws eks update-kubeconfig --name <EKS cluster name> --role-arn <Provision Role's ARN>`

### Deploy an application via GitLab CI/CD
1. On the local machine, retrieve the gitlab service account token with`kubectl describe secret gitlab-token`. 
1. Define the following environment variables under **Settings** -> **CI / CD** -> **Variables**:
    * `KUBE_NAMESPACE` = `<provide the targeted Kubernetes namespace>`. 
    * `DEPLOYMENT_FOLDER` = `deployment`
    * For following environment variables refer to the **Operation** -> **Kubernetes** -> `<select the targeted Kubernetes cluster>`. 
        * `API_URL` = `<The URL display under API URL textbox>` 
        * `CA_CERTIFICATE` = `<Base64 encoded value of the certificate found under CA Certificate textbox>`. 
            * Example: `echo "-----BEGIN CERTIFICATE----- XXXX -----END CERTIFICATE-----" | base64`
    * `GITLAB_TOKEN` =  `<gitlab service account token>`
![image](/uploads/d1238498f80b8b72639d8abc69ba0e5a/image.png)
1. Upload the repository to your gitlab project, e.g. `git push`, and visit the **CI / CD** page. 
1. Run the `Build` stage and follow by `Deploy` stage, to deploy the application to our EKS cluster. 
![image](/uploads/9bcc29e8d4f5e8dfd34670fe142e52c3/image.png)
1. From our local machine, query for the deployed application's **EXTERNAL-IP** with `kubectl get all -n <KUBE_NAMESPACE>` and verified the deploy application. 
![image](/uploads/4c735ecbfdc69b4f92f37c2b37c13d01/image.png)

## Observations
1. The remove integration option does not remove the associated AWS resources. Thus, we need to visit the AWS cloudformation and delete the associated stack. 

### References: 
1. https://aws.amazon.com/blogs/containers/ci-cd-with-amazon-eks-using-aws-app-mesh-and-gitlab-ci/
1. https://docs.gitlab.com/ee/user/project/clusters/#deploying-to-a-kubernetes-cluster
1. https://docs.gitlab.com/ee/user/project/deploy_keys/
1. https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html
1. https://sanderknape.com/2019/02/automated-deployments-kubernetes-gitlab/
1. https://kubernetes.io/docs/reference/kubectl/cheatsheet/
