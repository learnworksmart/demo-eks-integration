locals {
  policies = [
    # "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy",
    # "arn:aws:iam::aws:policy/AmazonEKSServicePolicy",
    aws_iam_policy.gitlab_eks_provision_role.arn
  ]
}

resource "aws_iam_role_policy_attachment" "gitlab_eks_provision_role" {
  depends_on  = [aws_iam_policy.gitlab_eks_provision_role]
  role        = aws_iam_role.gitlab_eks_provision_role.name
  count       = length(local.policies)
  policy_arn  = local.policies[count.index]
}

resource "aws_iam_role" "gitlab_eks_provision_role" {
  name = "${var.project}-provision-role"
  description = "This role is created for ${var.project} integration."
  assume_role_policy = data.aws_iam_policy_document.gitlab_eks_provision_role_iam_assume_role.json

  tags      = { 
    Name    = "${var.project}-provision-role"
    Project = var.project
  }
}

data "aws_iam_policy_document" "gitlab_eks_provision_role_iam_assume_role" {
  statement {
    principals {
      type        = "AWS"
      identifiers = [var.gitlab.account_id]
    }
    actions = ["sts:AssumeRole"]
    condition {
      test     = "StringEquals"
      variable = "sts:ExternalId"
      values   = [var.gitlab.external_id]
    }
  }
  statement {
    principals {
      type        = "AWS"
      identifiers = ["arn:aws:iam::367992932895:user/learnworksmart"]
    }
    actions = ["sts:AssumeRole"]
  }
}

resource "aws_iam_policy" "gitlab_eks_provision_role" {
  name        = "${var.project}-provision-role"
  policy      = data.aws_iam_policy_document.gitlab_eks_provision_role_iam_access_rights.json
  path        = "/"
  description = "This policy is created for ${var.project} integration."
}

data "aws_iam_policy_document" "gitlab_eks_provision_role_iam_access_rights" {
  statement {
      effect    = "Allow"
      actions   = ["autoscaling:*", 
        "cloudformation:*",
        "ec2:*",
        "eks:*",
        "iam:*",
        "ssm:*",
      ]
      resources = ["*"]    
  }
}

### AWS Service Role

resource "aws_iam_role_policy_attachment" "gitlab_eks_service_role" {
  role        = aws_iam_role.gitlab_eks_service_role.name
  policy_arn  = "arn:aws:iam::aws:policy/AmazonEKSClusterPolicy"
}

resource "aws_iam_role" "gitlab_eks_service_role" {
  name = "${var.project}-service-role"
  description = "This service role is created for ${var.project} integration."
  assume_role_policy = data.aws_iam_policy_document.gitlab_eks_service_role.json

  tags      = { 
    Name    = "${var.project}-service-role"
    Project = var.project
  }
}

data "aws_iam_policy_document" "gitlab_eks_service_role" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["eks.amazonaws.com"]
    }
  }
}

